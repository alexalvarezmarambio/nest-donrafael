import { CajaDto } from './dto/caja.dto';
import { Remesa } from './dto/remesa.dto';
import { SalidaDto } from './dto/salida.dto';
import { Cuadre } from './dto/cuadre.dto';
export declare class CajasService {
    obtenerCajaClave(clave: number): Promise<any>;
    obtenerCajasActivas(): Promise<any>;
    crearCaja(caja: CajaDto): Promise<any>;
    actualizarEstado(id: string, estado: string): Promise<any>;
    actualizarCuadre(id: string, cuadre: Cuadre): Promise<any>;
    actualizarRemesas(id: string, remesas: Remesa[]): Promise<any>;
    actualizarSalidas(id: string, salidas: SalidaDto[]): Promise<any>;
}
