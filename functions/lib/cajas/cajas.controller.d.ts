import { CajasService } from './cajas.service';
import { Hash } from '../shared/hash';
import { CajaDto } from './dto/caja.dto';
import { Remesa } from './dto/remesa.dto';
import { SalidaDto } from './dto/salida.dto';
import { Cuadre } from './dto/cuadre.dto';
export declare class CajasController {
    cajasService: CajasService;
    hash: Hash;
    constructor(cajasService: CajasService);
    obtenerCajaClave(hash: string, clave: number): Promise<any> | {
        error: string;
    };
    obtenerCajasActivas(hash: string): Promise<any> | {
        error: string;
    };
    crearCaja(hash: string, caja: CajaDto): Promise<any> | {
        error: string;
    };
    actualizarEstado(hash: string, id: string, estado: string): Promise<any> | {
        error: string;
    };
    actualizarCuadre(hash: string, id: string, cuadre: Cuadre): Promise<any> | {
        error: string;
    };
    actualizarRemesas(hash: string, id: string, remesas: Remesa[]): Promise<any> | {
        error: string;
    };
    actualizarSalidas(hash: string, salidas: SalidaDto[], id: string): Promise<any> | {
        error: string;
    };
}
