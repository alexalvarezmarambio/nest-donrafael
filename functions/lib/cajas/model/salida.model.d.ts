import { ProveedorModel } from "src/proveedores/model/proveedor.model";
export declare class SalidaModel {
    proveedor: ProveedorModel;
    monto: number;
}
