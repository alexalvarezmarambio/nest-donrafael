import { ProveedorModel } from "src/proveedores/model/proveedor.model";
export interface SalidaDto {
    proveedor: ProveedorModel;
    monto: number;
}
