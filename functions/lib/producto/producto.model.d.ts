export declare class ProductoModel {
    id?: string;
    barcode: string;
    category: string;
    cost: number;
    kilo: number;
    margin: number;
    name: string;
    neto: number;
    precio: number;
    stock: number;
}
