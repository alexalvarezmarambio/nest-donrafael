import { ProductoModel } from './producto.model';
export declare class ProductoService {
    obtenerProducto(barra: string): Promise<any>;
    obtenerProductos(): Promise<any>;
    obtenerCantidadProductos(): Promise<any>;
    obtenerProductoId(id: string): Promise<any>;
    crearProducto(body: ProductoModel): Promise<any>;
    actualizarProducto(id: string, body: any): Promise<any>;
    eliminarProducto(id: string): Promise<any>;
}
