import { ProductoService } from './producto.service';
import { ProductoModel } from './producto.model';
export declare class ProductoController {
    productoService: ProductoService;
    hash: string;
    constructor(productoService: ProductoService);
    obtenerProductos(hash: string): Promise<any> | Error;
    obtenerCantidadProductos(hash: string): Promise<any> | Error;
    obtenerProducto(hash: string, barra: string): Promise<any> | Error;
    obtenerProductoId(hash: string, id: string): Promise<any> | Error;
    crearProducto(hash: string, body: ProductoModel): Promise<any> | Error;
    actualizarProducto(hash: string, id: string, body: any): Promise<any> | Error;
    eliminarProducto(hash: string, id: string): Promise<any> | Error;
}
