export declare class Hash {
    private hash;
    constructor();
    getHash(): string;
    compareHash(hash: string): boolean;
}
