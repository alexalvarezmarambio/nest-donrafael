import { Caja } from "../../cajas/model/caja.model";
import { ProductoModel } from "../../producto/producto.model";
export interface VentaDto {
    fecha: string;
    folio: number;
    total: number;
    dte: number;
    caja: Caja;
    productos: ProductoModel[];
}
