import { VentaDto } from './dto/venta.dto';
export declare class VentasService {
    crearVenta(venta: VentaDto): Promise<any>;
    obtenerVentas(desde: string, hasta: string): Promise<any>;
    obtenerVentasCaja(id: string): Promise<any>;
}
