import { VentasService } from './ventas.service';
import { Hash } from '../shared/hash';
import { VentaDto } from './dto/venta.dto';
export declare class VentasController {
    ventaService: VentasService;
    hash: Hash;
    constructor(ventaService: VentasService);
    crearVenta(hash: string, venta: VentaDto): Promise<any> | {
        error: string;
    };
    obtenerVentas(hash: string, desde: string, hasta: string): Promise<any> | {
        error: string;
    };
    obtenerVentasCaja(hash: string, id: string): Promise<any> | {
        error: string;
    };
}
