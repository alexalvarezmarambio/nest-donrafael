import { UsuarioService } from './usuario.service';
import UsuarioLoginDto from './dto/usuario-login.dto';
export declare class UsuarioController {
    usuarioService: UsuarioService;
    hash: string;
    constructor(usuarioService: UsuarioService);
    obtenerUsuario(hash: string, id: string): Promise<any> | {
        Error: string;
    };
    authUsuario(hash: string, body: UsuarioLoginDto): Promise<any> | {
        Error: string;
    };
}
