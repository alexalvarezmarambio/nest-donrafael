import UsuarioLoginDto from './dto/usuario-login.dto';
export declare class UsuarioService {
    obtenerUsuario(id: string): Promise<any>;
    authUsuario(usuarioLogin: UsuarioLoginDto): Promise<any>;
}
