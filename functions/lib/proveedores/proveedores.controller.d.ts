import { Hash } from '../shared/hash';
import { ProveedoresService } from './proveedores.service';
import { ProveedorDto } from './dto/proveedor.interface';
export declare class ProveedoresController {
    proveedoresService: ProveedoresService;
    hash: Hash;
    constructor(proveedoresService: ProveedoresService);
    crearProveedor(hash: string, proveedor: ProveedorDto): Promise<any> | {
        error: string;
    };
    obtenerProveedores(hash: string): Promise<any> | {
        error: string;
    };
    actualizarProveedor(hash: string, id: string, proveedor: ProveedorDto): Promise<any> | {
        error: string;
    };
    eliminarProveedor(hash: string, id: string): Promise<any> | {
        error: string;
    };
}
