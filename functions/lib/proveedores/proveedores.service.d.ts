import { ProveedorDto } from './dto/proveedor.interface';
export declare class ProveedoresService {
    obtnerProveedores(): Promise<any>;
    crearProveedor(proveedor: ProveedorDto): Promise<any>;
    actualizarProveedor(id: string, proveedor: ProveedorDto): Promise<any>;
    eliminarProveedor(id: string): Promise<any>;
}
