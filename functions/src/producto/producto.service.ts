import { Injectable } from '@nestjs/common';
import { firestore } from '../database/firebase-admin';
import { ProductoModel } from './producto.model';

@Injectable()
export class ProductoService {

    async obtenerProducto(barra: string) {
        try {

            const result = await firestore.collection('products')
            .where('barcode', '==', barra).get();

            const response = result.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }));

            return response;
        } catch (error) {
           return error;
        }
    }

    async obtenerProductos() {
        try {
            const result = await firestore.collection('products')
            .orderBy('name').get();
            const response = result.docs.map( doc => ({
                id: doc.id,
                ...doc.data(),
            }) );
            return response;

        } catch (error) {
           return error;
        }
    }

    async obtenerCantidadProductos() {
        try {
            const documentRef = firestore.collection('products');
            const result = await documentRef.get();
            const response = result.docs.length;
            return response;

        } catch (error) {
           return error;
        }
    }

    async obtenerProductoId(id: string) {
        try {
            const result = await firestore.collection('products').doc(id).get();
            const response = {
                id: result.id,
                ...result.data(),
            }
            return response;
        } catch (error) {
            return error;
        }
    }

    async crearProducto(body: ProductoModel) {
        try {
            const result = await firestore.collection('products').doc().create(body);
            return result;
        } catch (error) {
            return error;
        }
    }

    async actualizarProducto(id: string, body: any) {
        try {
            const result = firestore.doc(`products/${id}`);
            const response = await result.update(body);
            return response;
        } catch (error) {
            return error;
        }
    }

    async eliminarProducto(id: string) {
        try {
            const result = firestore.doc(`products/${id}`);
            const response = await result.delete();
            return response;
        } catch (error) {
            return error;
        }
    }
}
