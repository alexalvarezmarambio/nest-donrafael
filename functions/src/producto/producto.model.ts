export class ProductoModel {
    public id?: string;
    public barcode: string;
    public category: string;
    public cost: number;
    public kilo: number;
    public margin: number;
    public name: string;
    public neto: number;
    public precio: number;
    public stock: number;
}