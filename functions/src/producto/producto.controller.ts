import { Controller, Body, Get, Headers, Param, Post, Put, Delete } from '@nestjs/common';
import { ProductoService } from './producto.service';
import { ProductoModel } from './producto.model';


@Controller('producto')
export class ProductoController {

    hash: string = 'cXBoYXJtYTIwMjA6WA==';

    constructor(
        public productoService: ProductoService
    ) {}

    @Get()
    obtenerProductos(@Headers('Authorization') hash: string) {
        if (this.hash !== hash) {
           return new Error('Bad Hash');
        }

        return this.productoService.obtenerProductos();
    }

    @Get('/total')
    obtenerCantidadProductos(@Headers('Authorization') hash: string) {
        if (this.hash !== hash) {
           return new Error('Bad Hash');
        }

        return this.productoService.obtenerCantidadProductos();
    }

    @Get('/:barra')
    obtenerProducto(@Headers('Authorization') hash: string, @Param('barra') barra: string) {

        if (this.hash !== hash) {
           return new Error('Bad Hash');
        }

        return this.productoService.obtenerProducto(barra);
    }

    @Get('/id/:id')
    obtenerProductoId(@Headers('Authorization') hash: string, @Param('id') id: string) {
        if (this.hash !== hash) {
           return new Error('Bad Hash');
        }

        return this.productoService.obtenerProductoId(id);
    }

    @Post()
    crearProducto(@Headers('Authorization') hash: string, @Body() body: ProductoModel) {
        if (this.hash !== hash) {
           return new Error('Bad Hash');
        }

        return this.productoService.crearProducto(body);
    }

    @Put('/:id')
    actualizarProducto(@Headers('Authorization') hash: string, @Param('id') id: string, @Body() body: any) {
        if (this.hash !== hash) {
           return new Error('Bad Hash');
        }

        return this.productoService.actualizarProducto(id, body);

    }

    @Delete('/:id')
    eliminarProducto(@Headers('Authorization') hash: string, @Param('id') id: string) {
        if (this.hash !== hash) {
           return new Error('Bad Hash');
        }

        return this.productoService.eliminarProducto(id);

    }


}
