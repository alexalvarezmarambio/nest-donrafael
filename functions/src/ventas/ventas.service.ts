import { Injectable } from '@nestjs/common';
import { firestore } from '../database/firebase-admin';
import { VentaDto } from './dto/venta.dto';


@Injectable()
export class VentasService {

    async crearVenta(venta: VentaDto) {
        try {
            const result = await firestore.collection('ventas').doc().create(venta);
            return result;
        } catch (error) {
            return error;
        }
    }

    async obtenerVentas(desde: string, hasta: string) {
        try {
            const result = await firestore.collection('ventas')
            .where('fecha', '>=', desde + ' 00:00:00')
            .where('fecha', '<=', hasta + ' 23:59:59')
            .get();

            const response = result.docs.map( (doc) => ({
                id: doc.id,
                ...doc.data()
            }) );

            return response;
        } catch (error) {
            return error;
        }
    }

    async obtenerVentasCaja(id: string) {
        try {
            const result = await firestore.collection('ventas')
            .where('caja.id', '==', id)
            .where('dte', '==', '39')
            .get();

            const response = result.docs.map( (doc) => ({
                id: doc.id,
                ...doc.data()
            }) );

            return response;

        } catch (error) {
            return error;
        }
    }
}
