import { Body, Controller, Headers, Post, Get, Param } from '@nestjs/common';
import { VentasService } from './ventas.service';
import { Hash } from '../shared/hash';
import { VentaDto } from './dto/venta.dto';

@Controller('ventas')
export class VentasController {

    hash = new Hash();

    constructor(
        public ventaService: VentasService
    ) {}

    @Post()
    crearVenta(@Headers('Authorization') hash: string, @Body() venta: VentaDto) {
        if (!this.hash.compareHash(hash)) {
            return {error: 'Bad Hash'};
        }

        return this.ventaService.crearVenta(venta);
    }

    @Get('/:desde/:hasta')
    obtenerVentas(@Headers('Authorization') hash: string, @Param('desde') desde: string, @Param('hasta') hasta: string) {
        if (!this.hash.compareHash(hash)) {
            return {error: 'Bad Hash'};
        }

        return this.ventaService.obtenerVentas(desde, hasta);
    }

    @Get('/:id')
    obtenerVentasCaja(@Headers('Authorization') hash: string, @Param('id') id: string) {
        if (!this.hash.compareHash(hash)) {
            return {error: 'Bad Hash'};
        }

        return this.ventaService.obtenerVentasCaja(id);
    }
}
