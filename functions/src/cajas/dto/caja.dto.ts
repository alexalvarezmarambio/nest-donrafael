import { Vendedor } from "../model/vendedor.model";
import {Remesa} from "./remesa.dto";
import {SalidaModel} from "../model/salida.model";

export interface CajaDto {
  apertura: string;
  cierre: string;
  fondo: number;
  estado: string;
  vendedor: Vendedor;
  remesas: Remesa[];
  salidas: SalidaModel[];
}
