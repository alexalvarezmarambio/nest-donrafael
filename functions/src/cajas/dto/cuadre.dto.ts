export interface Cuadre {
  ventas: number;
  remesas: number;
  salidas: number;
  sistema: number;
  vendedor: number;
  diferencia: number;
}
