import { Injectable } from '@nestjs/common';
import { firestore } from '../database/firebase-admin';
import {CajaDto} from './dto/caja.dto';
import {Remesa} from './dto/remesa.dto';
import {SalidaDto} from './dto/salida.dto';
import {Cuadre} from './dto/cuadre.dto';


@Injectable()
export class CajasService {

    async obtenerCajaClave(clave: number) {
        try {
            const result = await firestore.collection('boxes')
            .where('estado', '==', '0')
            .where('vendedor.clave', '==', clave).get();
            const res = result.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }));
            return res;
        } catch (error) {
            return error;
        }
    }

    async obtenerCajasActivas() {
        try {

            const result = await firestore.collection('boxes')
            .where('estado', '<', '2').get();

            const res = result.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }));

            return res;

        } catch (error) {
            return error;
        }
    }

    async crearCaja(caja: CajaDto) {
        try {
            await firestore.collection('boxes').doc().create(caja);
        } catch (error) {
            return error;
        }
    }

    async actualizarEstado(id:string, estado: string) {
        try {
            await firestore.doc(`boxes/${ id }`).update({
                estado: estado
            });
        } catch (error) {
            return error;
        }
    }

    async actualizarCuadre(id:string, cuadre: Cuadre) {
        try {
            await firestore.doc(`boxes/${ id }`).update({
                cuadre
            });
        } catch (error) {
            return error;
        }
    }

    async actualizarRemesas(id: string, remesas: Remesa[]) {
        try {
            await firestore.doc(`boxes/${ id }`).update({
                remesas
            });
        } catch (error) {
            return error;
        }
    }
    
    async actualizarSalidas(id: string, salidas: SalidaDto[]) {
        try {
            await firestore.doc(`boxes/${ id }`).update({
                salidas
            })
        } catch (error) {
            return error;
        }
    }
}
