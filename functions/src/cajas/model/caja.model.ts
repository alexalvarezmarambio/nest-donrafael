import { Vendedor } from "./vendedor.model";
import {RemesaModel} from "./remesa.model";
import {SalidaModel} from "./salida.model";

export interface Caja {
  id: string;
  apertura: string;
  cierre: string;
  fondo: number;
  estado: string;
  vendedor: Vendedor;
  remesas: RemesaModel[];
  salidas: SalidaModel[];
}
