import {ProveedorModel} from "src/proveedores/model/proveedor.model";

export class SalidaModel {
  proveedor: ProveedorModel;
  monto: number;
}
