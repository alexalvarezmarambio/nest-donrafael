import { Module } from '@nestjs/common';
import { CajasService } from './cajas.service';
import { CajasController } from './cajas.controller';

@Module({
  providers: [CajasService],
  controllers: [CajasController]
})
export class CajasModule {}
