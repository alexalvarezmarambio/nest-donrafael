import { Controller, Get, Headers, Param, Post, Put, Body } from '@nestjs/common';
import { CajasService } from './cajas.service';
import { Hash } from '../shared/hash';
import {CajaDto} from './dto/caja.dto';
import {Remesa} from './dto/remesa.dto';
import {SalidaDto} from './dto/salida.dto';
import {Cuadre} from './dto/cuadre.dto';


@Controller('cajas')
export class CajasController {

    hash = new Hash();

    constructor(
        public cajasService: CajasService
    ) {}

    @Get('/clave/:clave')
    obtenerCajaClave(@Headers('Authorization') hash: string, @Param('clave') clave: number) {
        if (!this.hash.compareHash(hash)) {
            return {"error":"Bad Hash"};
        }

        return this.cajasService.obtenerCajaClave(clave);
    }

    @Get('/activas')
    obtenerCajasActivas(@Headers('Authorization') hash: string) {
        if (!this.hash.compareHash(hash)) {
            return {"error":"Bad Hash"};
        }

        return this.cajasService.obtenerCajasActivas();
    }

    @Post()
    crearCaja(@Headers('Authorization') hash: string, @Body() caja: CajaDto) {
        if (!this.hash.compareHash(hash)) {
            return {"error":"Bad Hash"};
        }

        return this.cajasService.crearCaja(caja);
    }

    @Put('/estado/:id/:estado')
    actualizarEstado(
        @Headers('Authorization') hash: string, @Param('id') id: string, @Param('estado') estado: string
    ) {
        if (!this.hash.compareHash(hash)) {
            return {"error":"Bad Hash"};
        }

        return this.cajasService.actualizarEstado(id, estado);
    }

    @Put('/cuadre/:id')
    actualizarCuadre(
        @Headers('Authorization') hash: string, @Param('id') id: string, @Body() cuadre: Cuadre
    ) {
        if (!this.hash.compareHash(hash)) {
            return {"error":"Bad Hash"};
        }

        return this.cajasService.actualizarCuadre(id, cuadre);
    }

    @Put('/remesa/:id')
    actualizarRemesas(@Headers('Authorization') hash: string, @Param('id') id: string, @Body() remesas: Remesa[]) {
        if (!this.hash.compareHash(hash)) {
            return {"error":"Bad Hash"};
        }

        return this.cajasService.actualizarRemesas(id, remesas);
    }

    @Put('/salida/:id')
    actualizarSalidas(@Headers('Authorization') hash: string, @Body() salidas: SalidaDto[], @Param('id') id: string) {
        if (!this.hash.compareHash(hash)) {
            return {"error":"Bad Hash"};
        }

        return this.cajasService.actualizarSalidas(id, salidas);
    }
}
