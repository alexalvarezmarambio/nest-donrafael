import { Controller, Headers, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import {Hash} from '../shared/hash';
import {ProveedoresService} from './proveedores.service';
import {ProveedorDto} from './dto/proveedor.interface';

@Controller('proveedores')
export class ProveedoresController {

  hash = new Hash();

  constructor(
    public proveedoresService: ProveedoresService
  ) {}

  @Post()
  crearProveedor(@Headers('Authorization') hash: string, @Body() proveedor: ProveedorDto) {
    if (!this.hash.compareHash(hash)) {
      return  {"error": "Bad Hash"};
    }

    return this.proveedoresService.crearProveedor(proveedor);
  }

  @Get()
  obtenerProveedores(@Headers('Authorization') hash: string) {
    if (!this.hash.compareHash(hash)) {
      return  {"error": "Bad Hash"};
    }

    return this.proveedoresService.obtnerProveedores();

  }

  @Put('/:id')
  actualizarProveedor(@Headers('Authorization') hash: string, @Param('id') id: string, @Body() proveedor: ProveedorDto) {
    if (!this.hash.compareHash(hash)) {
      return  {"error": "Bad Hash"};
    }

    return this.proveedoresService.actualizarProveedor(id, proveedor);
  }

  @Delete('/:id')
  eliminarProveedor(@Headers('Authorization') hash: string, @Param('id') id: string) {
    if (!this.hash.compareHash(hash)) {
      return  {"error": "Bad Hash"};
    }

    return this.proveedoresService.eliminarProveedor(id);
  }

}
