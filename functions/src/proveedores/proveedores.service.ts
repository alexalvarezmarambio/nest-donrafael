import { Injectable } from '@nestjs/common';
import { firestore } from '../database/firebase-admin';
import {ProveedorDto} from './dto/proveedor.interface';

@Injectable()
export class ProveedoresService {

  async obtnerProveedores() {

    try {
      const collectionRef = firestore.collection('proveedores').orderBy('nombre', 'asc');
      const result = await collectionRef.get();
      const res = result.docs.map((doc) => ({
        id: doc.id,
        ...doc.data()
      }));

      return res;

    } catch (error) {
      return error;
    }

  }

  async crearProveedor(proveedor: ProveedorDto) {
    try {
      const documentRef = firestore.collection('proveedores').doc();
      const result = await documentRef.create(proveedor);
      return result;

    } catch (error) {
      return error;
    }
  }

  async actualizarProveedor(id: string, proveedor: ProveedorDto) {
    try {
      const documentRef = firestore.doc(`proveedores/${id}`);
      const result = await documentRef.update(proveedor);
      return result;

    } catch (error) {
      return error;
    }
  }

  async eliminarProveedor(id: string) {
    try {
      const documentRef = firestore.doc(`proveedores/${id}`);
      const result = await documentRef.delete();
      return result;

    } catch (error) {
      return error;
    }
  }
}
