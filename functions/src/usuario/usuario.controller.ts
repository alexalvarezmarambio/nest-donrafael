import { Controller, Get, Post, Headers, Param, Body } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import UsuarioLoginDto from './dto/usuario-login.dto';


@Controller('usuario')
export class UsuarioController {

    hash: string = 'cXBoYXJtYTIwMjA6WA==';

    constructor(
        public usuarioService: UsuarioService
    ) {}

    @Get('/:id')
    obtenerUsuario(@Headers('Authorization') hash: string, @Param('id') id: string) {
        if (this.hash !== hash) {
            return {"Error": "Bad Hash"};
        }

        return this.usuarioService.obtenerUsuario(id);

    }

    @Post('/login')
    authUsuario(
        @Headers('Authorization') hash: string,
        @Body() body: UsuarioLoginDto
    ) {
        if (this.hash !== hash) {
            return {"Error": "Bad Hash"};
        }

        return this.usuarioService.authUsuario(body);


    }
}
