import { Injectable } from '@nestjs/common';
import { firestore } from '../database/firebase-admin';
import UsuarioLoginDto from './dto/usuario-login.dto';

@Injectable()
export class UsuarioService {

    async obtenerUsuario(id: string) {
        try {
            const doc = await firestore.collection('usuarios').doc(id).get();
            const response = {
                id: doc.id,
                ...doc.data(),
            };
            return response;
        } catch (error) {
           return error;
        }

    }

    async authUsuario(usuarioLogin: UsuarioLoginDto) {
        try {
            const result = await firestore.collection('usuarios')
                              .where('usuario', '==', usuarioLogin.usuario)
                              .where('clave', '==', usuarioLogin.clave).get();

            const response = result.docs.map((doc) => ({
                id: doc.id,
                ...doc.data(),
            }));

            return response;

        } catch (error) {
           return error;
        }
    }
}
