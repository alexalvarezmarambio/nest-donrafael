interface UsuarioDto {
  nombre: string,
  email: string,
  usuario: string,
  clave: string
}

export default UsuarioDto;
