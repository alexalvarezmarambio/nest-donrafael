import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { UsuarioModule } from './usuario/usuario.module';
import { ProductoModule } from './producto/producto.module';
import { AuthModule } from './auth/auth.module';
import { CajasModule } from './cajas/cajas.module';
import { VentasModule } from './ventas/ventas.module';
import { ProveedoresModule } from './proveedores/proveedores.module';

@Module({
  controllers: [AppController],
  imports: [UsuarioModule, ProductoModule, AuthModule, CajasModule, VentasModule, ProveedoresModule],
})
export class AppModule {}
