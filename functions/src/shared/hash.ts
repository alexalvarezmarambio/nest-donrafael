export class Hash {
    private hash: string;

    constructor() {
        this.hash = 'cXBoYXJtYTIwMjA6WA=='
    }

    getHash(): string {
        return this.hash;
    }

    compareHash(hash: string): boolean {
        return this.hash === hash;
    }


}